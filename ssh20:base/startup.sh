#! /bin/bash

# afegim usuaris
useradd unix01
useradd unix02
useradd unix03
echo -e "unix01\nunix01\n" | passwd unix01 --stdin
echo -e "unix02\nunix02\n" | passwd unix02 --stdin
echo -e "unix03\nunix03\n" | passwd unix03 --stdin

# Serveis
cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/system-auth /etc/pam.d/system-auth
cp /opt/docker/system-auth /etc/pam.d/password-auth

# encenem serveis nscd i nslcd
/sbin/nscd
/sbin/nslcd

#creem clais i serveis
/usr/bin/ssh-keygen -A
/sbin/sshd -D
#/bin/bash
